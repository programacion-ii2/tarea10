/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.empleado;

/**
 *
 * @author USUARIO
 */
import java.util.Scanner; 
public class Main {
    public static void main(String[] args) {
        EmployeeManager employeeManager = new EmployeeManager();
        Scanner scanner = new Scanner(System.in);

        int opcion;

        do {
            System.out.println("Lista Empleados ");
            System.out.println("1. Agregar empleado");
            System.out.println("2. Mostrar empleados");
            System.out.println("3. Actualizar empleado");
            System.out.println("4. Eliminar empleado");
            System.out.println("5. Buscar empleado");
            System.out.println("6. Salir");

            System.out.print("Ingrese su opción: ");
            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    agregarEmpleado(employeeManager, scanner);
                    break;
                case 2:
                    employeeManager.mostrarEmpleados();
                    break;
                case 3:
                    actualizarEmpleado(employeeManager, scanner);
                    break;
                case 4:
                    eliminarEmpleado(employeeManager, scanner);
                    break;
                case 5:
                    buscarEmpleado(employeeManager, scanner);
                    break;
                case 6:
                    System.out.println("Saliendo");
                    break;
                default:
                    
            }

        } while (opcion != 0);
    }

        static void agregarEmpleado(EmployeeManager employeeManager, Scanner scanner) {
        System.out.print("Ingrese el nombre del empleado: ");
        String nombre = scanner.nextLine();

        System.out.print("Ingrese el apellido del empleado: ");
        String apellido = scanner.nextLine();

        System.out.print("Ingrese la edad del empleado: ");
        int edad = scanner.nextInt();
        scanner.nextLine(); 

        System.out.print("Ingrese el salario del empleado: ");
        double salario = scanner.nextDouble();
        scanner.nextLine(); 

        System.out.print("Ingrese el puesto del empleado: ");
        String puesto = scanner.nextLine();

        System.out.print("Ingrese el departamento del empleado: ");
        String departamento = scanner.nextLine();

        Empleado nuevoEmpleado = new Empleado(nombre, apellido, edad, salario, puesto, departamento);
        employeeManager.agregarEmpleado(nuevoEmpleado);
        System.out.println("Empleado agregado con éxito.");
    }

    static void actualizarEmpleado(EmployeeManager employeeManager, Scanner scanner) {
        System.out.print("Ingrese el nombre del empleado que desea actualizar: ");
        String nombre = scanner.nextLine();

        

        Empleado nuevoEmpleado = obtenerDatosEmpleadoDesdeUsuario(scanner);
        employeeManager.actualizarEmpleado(nombre, nuevoEmpleado);
    }

    static void eliminarEmpleado(EmployeeManager employeeManager, Scanner scanner) {
        System.out.print("Ingrese el nombre del empleado que desea eliminar: ");
        String nombre = scanner.nextLine();

        employeeManager.eliminarEmpleado(nombre);
    }

     static void buscarEmpleado(EmployeeManager employeeManager, Scanner scanner) {
        System.out.print("Ingrese el criterio de búsqueda (nombre, puesto, salario, etc.): ");
        String criterio = scanner.nextLine();

        System.out.print("Ingrese el valor a buscar: ");
        String valor = scanner.nextLine();

        employeeManager.buscarEmpleadoPorCriterio(criterio, valor);
    }

    static Empleado obtenerDatosEmpleadoDesdeUsuario(Scanner scanner) {
        System.out.print("Ingrese el nombre del empleado: ");
        String nombre = scanner.nextLine();

        System.out.print("Ingrese el apellido del empleado: ");
        String apellido = scanner.nextLine();

        System.out.print("Ingrese la edad del empleado: ");
        int edad = scanner.nextInt();

        System.out.print("Ingrese el salario del empleado: ");
        double salario = scanner.nextDouble();
        scanner.nextLine(); 

        System.out.print("Ingrese el puesto del empleado: ");
        String puesto = scanner.nextLine();

        System.out.print("Ingrese el departamento del empleado: ");
        String departamento = scanner.nextLine();

        return new Empleado(nombre, apellido, edad, salario, puesto, departamento);
    }
    
}
