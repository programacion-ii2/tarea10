/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.empleado;

/**
 *
 * @author USUARIO
 * 
 * 
 */
import java.util.ArrayList;
import java.util.List;
public class EmployeeManager {
    
    private List<Empleado> listaEmpleados;

    public EmployeeManager() {
        this.listaEmpleados = new ArrayList<>();
    }

    public void agregarEmpleado(Empleado empleado) {
        listaEmpleados.add(empleado);
    }

    public void mostrarEmpleados() {
        if (listaEmpleados.isEmpty()) {
            System.out.println("No hay empleados en la lista.");
        } else {
            System.out.println("Lista de Empleados");
            for (int i = 0; i < listaEmpleados.size(); i++) {
                Empleado empleado = listaEmpleados.get(i);
                System.out.println((i + 1) + ". " + empleado);
            }
        }
    }

    public void actualizarEmpleado(String nombre, Empleado nuevoEmpleado) {
        boolean encontrado = false;
        for (int i = 0; i < listaEmpleados.size(); i++) {
            Empleado empleado = listaEmpleados.get(i);
            if (empleado.getNombre().equals(nombre)) {
                listaEmpleados.set(i, nuevoEmpleado);
                encontrado = true;
                System.out.println("Empleado actualizado .");
                break;
            }
        }
        if (!encontrado) {
            System.out.println("No se encontró un empleado .");
        }
    }

    public void eliminarEmpleado(String nombre) {
        boolean encontrado = false;
        for (int i = 0; i < listaEmpleados.size(); i++) {
            Empleado empleado = listaEmpleados.get(i);
            if (empleado.getNombre().equals(nombre)) {
                listaEmpleados.remove(i);
                encontrado = true;
                System.out.println("Empleado eliminado con éxito.");
                break;
            }
        }
        if (!encontrado) {
            System.out.println("Empleado no encontrado .");
        }
    }

    public void buscarEmpleadoPorCriterio(String criterio, String valor) {
    System.out.println(" Buscando empleado " + criterio + " ");
    boolean encontrado = false;

    for (Empleado empleado : listaEmpleados) {
        if ("nombre".equals(criterio) && empleado.getNombre().equals(valor)) {
            empleado.mostrarDetalles();
            encontrado = true;
        } else if ("puesto".equals(criterio) && empleado.getPuesto().equals(valor)) {
            empleado.mostrarDetalles();
            encontrado = true;
        } else if ("salario".equals(criterio) && Double.toString(empleado.getSalario()).equals(valor)) {
            empleado.mostrarDetalles();
            encontrado = true;
        }
        
    }

    if (!encontrado) {
        System.out.println("No se encontró un empleado ");
    }
}
}
