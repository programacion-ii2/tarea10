/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.libroo;

/**
 *
 * @author USUARIO
 */

import java.util.Scanner;
public class Main {

public static void main(String[] args) {
        LibraryManager libraryManager = new LibraryManager();
        Scanner scanner = new Scanner(System.in);

        int opcion;

        do {
            displayMenu();
            System.out.print("Ingrese su opción ");
            String input = scanner.nextLine();

            try {
                opcion = Integer.parseInt(input);
            } catch (NumberFormatException e) {
                opcion = -1;
            }

            switch (opcion) {
                case 1:
                    agregarLibro(libraryManager, scanner);
                    break;
                case 2:
                    libraryManager.showBooks();
                    break;
                case 3:
                    actualizarLibro(libraryManager, scanner);
                    break;
                case 4:
                    eliminarLibro(libraryManager, scanner);
                    break;
                case 5:
                    buscarLibroPorTitulo(libraryManager, scanner);
                    break;
                default:
                    if (opcion != 0) {
                        System.out.println("error");
                    }
            }

        } while (opcion != 0);
    }

    public static void displayMenu() {
        System.out.println("Menu");
        System.out.println("1. Agregar libro");
        System.out.println("2. Mostrar libros");
        System.out.println("3. Actualizar libro");
        System.out.println("4. Eliminar libro");
        System.out.println("5. Buscar libro por título");
        System.out.println("0. Salir");
    }

    public static void agregarLibro(LibraryManager libraryManager, Scanner scanner) {
        System.out.print("Ingrese el título del libro: ");
        String titulo = scanner.nextLine();

        System.out.print("Ingrese el autor del libro: ");
        String autor = scanner.nextLine();

        System.out.print("Ingrese el género del libro: ");
        String genero = scanner.nextLine();

        System.out.print("Ingrese el número de páginas del libro: ");
        int numeroPaginas = scanner.nextInt();
        scanner.nextLine(); 

        System.out.print("Ingrese el ISBN del libro: ");
        String isbn = scanner.nextLine();

        System.out.print("Ingrese el precio del libro: ");
        double precio = scanner.nextDouble();
        scanner.nextLine(); 

        Libroo nuevoLibro = new Libroo(titulo, autor, genero, numeroPaginas, isbn, precio);
        libraryManager.addBook(nuevoLibro);
        System.out.println("Libro agregado con éxito.");
    }

    public static void actualizarLibro(LibraryManager libraryManager, Scanner scanner) {
        System.out.print("Ingrese el ISBN del libro : ");
        String isbn = scanner.nextLine();

        System.out.print("Ingrese el nuevo título del libro: ");
        String nuevoTitulo = scanner.nextLine();

        System.out.print("Ingrese el nuevo autor del libro: ");
        String nuevoAutor = scanner.nextLine();

        System.out.print("Ingrese el nuevo género del libro: ");
        String nuevoGenero = scanner.nextLine();

        System.out.print("Ingrese el nuevo número de páginas del libro: ");
        int nuevoNumeroPaginas = scanner.nextInt();
        scanner.nextLine(); 

        System.out.print("Ingrese el nuevo precio del libro: ");
        double nuevoPrecio = scanner.nextDouble();
        

        Libroo nuevoLibro = new Libroo(nuevoTitulo, nuevoAutor, nuevoGenero, nuevoNumeroPaginas, isbn, nuevoPrecio);
        libraryManager.updateBook(isbn, nuevoLibro);
        System.out.println("Libro actualizado con éxito.");
    }

    public static void eliminarLibro(LibraryManager libraryManager, Scanner scanner) {
        System.out.print("Ingrese el ISBN del libro que desea eliminar: ");
        String isbn = scanner.nextLine();
        libraryManager.eliminarLibro(isbn);
        System.out.println("Libro eliminado con éxito.");
    }

    public static void buscarLibroPorTitulo(LibraryManager libraryManager, Scanner scanner) {
        System.out.print("Ingrese el título del libro que desea buscar: ");
        String titulo = scanner.nextLine();
        libraryManager.findBookByTitle(titulo);
    }
}

