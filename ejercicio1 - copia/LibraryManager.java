/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.libroo;

/**
 *
 * @author USUARIO
 */
import java.util.ArrayList;
import java.util.List;
public class LibraryManager {
    private List<Libroo> listaLibros;

    
    public LibraryManager() {
        this.listaLibros = new ArrayList<>();
    }

    public void addBook(Libroo libro) {
        listaLibros.add(libro);
        
    }

    
    public void showBooks() {
        System.out.println("Detalles de los libros alamacenados :");
        for (Libroo libro : listaLibros) {
            libro.mostrarDetalles();
        }
    }

    
    public void updateBook(String isbn, Libroo nuevoLibro) {
        for (int i = 0; i < listaLibros.size(); i++) {
            if (listaLibros.get(i).getIsbn().equals(isbn)) {
                listaLibros.set(i, nuevoLibro);
                System.out.println("Libro actualizo.");
                return;
            }
        }
    }

    
    public void eliminarLibro(String isbn) {
        for (Libroo libro : listaLibros) {
            if (libro.getIsbn().equals(isbn)) {
                listaLibros.remove(libro);
                System.out.println("Libro eliminado ");
                return;
            }
        }
        
    }

    
    public void findBookByTitle(String titulo) {
        boolean encontrado = false;
        for (Libroo libro : listaLibros) {
            if (libro.getTitulo().equals(titulo)) {
                System.out.println("Detalles del libro :");
                libro.mostrarDetalles();
                encontrado = true;
                break;
            }
        }

        if (!encontrado) {
            System.out.println("libro no encontrado.");
        }
    }
    
}
