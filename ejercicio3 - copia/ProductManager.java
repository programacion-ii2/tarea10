/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.producto;

/**
 *
 * @author USUARIO
 */
import java.util.ArrayList;
import java.util.List;
public class ProductManager {
    
    private List<Producto> listaProductos;

    public ProductManager() {
        this.listaProductos = new ArrayList<>();
    }

    public void addProduct(Producto producto) {
        listaProductos.add(producto);
        System.out.println("Producto agregado.");
    }

    public void showProducts() {
        if (listaProductos.isEmpty()) {
            System.out.println("No hay producto.");
        } else {
            System.out.println(" Lista de Productos ");
            for (Producto producto : listaProductos) {
                producto.mostrarDetalles();
                
            }
        }
    }

    public void updateProduct(int id, Producto nuevoProducto) {
        boolean encontrado = false;
        for (int i = 0; i < listaProductos.size(); i++) {
            Producto producto = listaProductos.get(i);
            if (producto.getId() == id) {
                listaProductos.set(i, nuevoProducto);
                encontrado = true;
                System.out.println("Producto actualizado ");
                break;
            }
        }
        if (!encontrado) {
            System.out.println("No se encontró un producto ");
        }
    }

    public void eliminarProducto(int id) {
        boolean encontrado = false;
        for (int i = 0; i < listaProductos.size(); i++) {
            Producto producto = listaProductos.get(i);
            if (producto.getId() == id) {
                listaProductos.remove(i);
                encontrado = true;
                System.out.println("Producto eliminado ");
                break;
            }
        }
        if (!encontrado) {
            System.out.println("No se encontró un producto ");
        }
    }

    public void searchProductByName(String nombre) {
        System.out.println("=== Búsqueda de Producto por Nombre ===");
        boolean encontrado = false;
        for (Producto producto : listaProductos) {
            if (producto.getNombre().equalsIgnoreCase(nombre)) {
                producto.mostrarDetalles();
                encontrado = true;
                break;
            }
        }
        if (!encontrado) {
            System.out.println("No se encontró un producto ");
        }
    }
    
}
