/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.producto;

/**
 *
 * @author USUARIO
 */
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ProductManager productManager = new ProductManager();

        int opcion;

        do {
            mostrarMenu();
            opcion = obtenerOpcionUsuario(scanner);

            switch (opcion) {
                case 1:
                    agregarProducto(productManager, scanner);
                    break;
                case 2:
                    mostrarProductos(productManager);
                    break;
                case 3:
                    actualizarProducto(productManager, scanner);
                    break;
                case 4:
                    eliminarProducto(productManager, scanner);
                    break;
                case 5:
                    buscarProductoPorNombre(productManager, scanner);
                    break;
                case 6:
                    System.out.println("Saliendo del programa. ¡Hasta luego!");
                    break;
                default:
                   
            }

        } while (opcion != 0);
    }

    static void mostrarMenu() {
        System.out.println("Sistema de Gestión de Productos ");
        System.out.println("1. Agregar producto");
        System.out.println("2. Mostrar productos");
        System.out.println("3. Actualizar producto");
        System.out.println("4. Eliminar producto");
        System.out.println("5. Buscar producto ");
        System.out.println("6. Salir");
    }

    private static int obtenerOpcionUsuario(Scanner scanner) {
        System.out.print("Ingrese su opción: ");
        return scanner.nextInt();
    }

    private static void agregarProducto(ProductManager productManager, Scanner scanner) {
        System.out.println(" Agregar Producto ");

        System.out.print("Ingrese el ID del producto: ");
        int id = scanner.nextInt();
        
        System.out.print("Ingrese el nombre del producto: ");
        String nombre = scanner.nextLine();

        System.out.print("Ingrese la descripción del producto: ");
        String descripcion = scanner.nextLine();

        System.out.print("Ingrese el precio del producto: ");
        double precio = scanner.nextDouble();
        
        System.out.print("Ingrese la cantidad del producto: ");
        int cantidad = scanner.nextInt();
       

        System.out.print("Ingrese la categoría del producto: ");
        String categoria = scanner.nextLine();

        Producto nuevoProducto = new Producto(id, nombre, descripcion, precio, cantidad, categoria);
        productManager.addProduct(nuevoProducto);

        System.out.println("Producto agregado con éxito.\n");
    }

    private static void mostrarProductos(ProductManager productManager) {
        System.out.println(" Mostrar Productos ");
        productManager.showProducts();
        System.out.println();
    }

    private static void actualizarProducto(ProductManager productManager, Scanner scanner) {
        System.out.println(" Actualizar Producto ");

        System.out.print("Ingrese el ID del producto que desea actualizar: ");
        int id = scanner.nextInt();
        

        Producto nuevoProducto = obtenerDatosProductoDesdeUsuario(scanner);
        productManager.updateProduct(id, nuevoProducto);

        System.out.println("Producto actualizado con éxito.\n");
    }

    private static void eliminarProducto(ProductManager productManager, Scanner scanner) {
        System.out.println(" Eliminar Producto ");

        System.out.print("Ingrese el ID del producto que desea eliminar: ");
        int id = scanner.nextInt();
        
        productManager.eliminarProducto(id);

        System.out.println("Producto eliminado con éxito.\n");
    }

    private static void buscarProductoPorNombre(ProductManager productManager, Scanner scanner) {
        System.out.println(" Buscar Producto por Nombre ");

        System.out.print("Ingrese el nombre del producto que desea buscar: ");
        String nombre = scanner.nextLine();

        productManager.searchProductByName(nombre);

        System.out.println();
    }

    private static Producto obtenerDatosProductoDesdeUsuario(Scanner scanner) {
        System.out.print("Ingrese el nombre del producto: ");
        String nombre = scanner.nextLine();

        System.out.print("Ingrese la descripción del producto: ");
        String descripcion = scanner.nextLine();

        System.out.print("Ingrese el precio del producto: ");
        double precio = scanner.nextDouble();
        
        System.out.print("Ingrese la cantidad del producto: ");
        int cantidad = scanner.nextInt();
        
        System.out.print("Ingrese la categoría del producto: ");
        String categoria = scanner.nextLine();

        return new Producto(0, nombre, descripcion, precio, cantidad, categoria);
    }
    
}
